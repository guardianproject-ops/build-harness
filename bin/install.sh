#!/bin/bash
export BUILD_HARNESS_ORG=${1:-guardianproject-ops}
export BUILD_HARNESS_PROJECT=${2:-build-harness}
export BUILD_HARNESS_BRANCH=${3:-master}
export GITLAB_REPO="https://gitlab.com/${BUILD_HARNESS_ORG}/${BUILD_HARNESS_PROJECT}.git"

if [ "$BUILD_HARNESS_PROJECT" ] && [ -d "$BUILD_HARNESS_PROJECT" ]; then
  echo "Removing existing $BUILD_HARNESS_PROJECT"
  rm -rf "$BUILD_HARNESS_PROJECT"
fi

echo "Cloning ${GITLAB_REPO}#${BUILD_HARNESS_BRANCH}..."
git clone -c advice.detachedHead=false --depth=1 -b $BUILD_HARNESS_BRANCH $GITLAB_REPO
